##Tracking Code
-> Grundeinstellungen -> Tracking Code
<br><br>
In der Kachel Tracking Code können Sie Code-Snipsel wie bspw. von Google (Analytics, Tag Manager,...) oder anderen Anbietern integrieren.
Hierfür stehen Ihnen zwei Felder zur Verfügung:

* Code Head
    * Hier fügen Sie den Quellcode ein, der in den Headbereich ( ```<head>...</head>```) der Seite eingebunden werden soll. Zu beachten ist hier, dass Ihr Code in einem script-Tag ( ```<script>...</script>```) stehen muss.
* Code Body
    * Wenn der Anbieter möchte, dass Quellcode in den Bodybereich ( ```<body>...</body>```) der Seite eingebunden werden soll, dann fügen Sie den in dieses Feld ein. Zu beachten ist hier, dass der Code in einem noscript-Tag ( ```<noscript>...</noscript>```) stehen muss. 
    
<div class='warning'>
    Hinweis: Wenn Sie bisher die Kachel Tracking ID verwendet haben, ist das kein Problem. Diese Kachel wurde ersetzt. Ihre Tracking ID wird wie bisher auch, als Google Analytics Code integriert. Sie sehen den Code, wenn Sie die Kachel Tracking Code öffnen.
</div>
<div class='attention'>
    ACHTUNG: Wenn Sie Tracking Code einbinden müssen Sie prüfen, ob Sie eine Opt-In Cookie Meldung benötigen. Mehr erfahren Sie <a href="#cookie_meldung" id="cookie_meldung" data-url="./2-Grundeinstellungen/2-Cookie_Meldung.md" class="js_doc_link">hier</a>.
</div>
