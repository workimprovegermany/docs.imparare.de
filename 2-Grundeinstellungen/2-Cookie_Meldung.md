##Cookie Meldung
-> Grundeinstellungen -> Cookie Meldung
<br><br>
Mit der DSGVO wurden auch die Richtlinien für Cookies angepasst. Die sog. Opt-In Cookie Meldungen holen erst die Erlaubnis des Users ein, Cookies verwenden zu dürfen. Gibt der User seine Zustimmung, so wird der Tracking Code nachgelagert integriert. Verweigert der User, so wird auch kein Tracking Code nachgeladen. 
Wenn Sie Tracking Code einbinden, dann geben Sie immer Daten an Dritte weiter. Hier handelt es sich in den meisten Fällen um Third Party Cookies. Diese bedingen eine Zustimmung des Users.
<div class='attention'>
    ACHTUNG: Wir können hier keine rechtsverbindliche Antwort geben, ob Sie eine Cookie Meldung benötigen. Bitte lassen Sie es von einem Anwalt prüfen, wenn Sie sich nicht sicher sind.
</div> 

Über unsere Kachel 'Cookie Meldung' können Sie sowohl Farben als auch Ihren Text individuell gestalten. <br>Folgende Optionen stehen Ihnen zur Auswahl:
* Cookie Meldung aktiv
    * Hier legen Sie fest, ob eine Cookie Meldung beim Besuchen Ihrer Subdomain erscheinen soll. Die Cookie Meldungen sind immer mit Opt-In. Wenn Sie diese Option deaktivieren wird Ihr Tracking Code (wenn vorhanden) sofort integriert. Achtung, hier gilt der Hinweis von oben.
* Cookie Text
    * Hier können Sie Ihre persönliche Cookie Meldung angeben und einen Link zu Ihrer Datenschutzerklärung hinterlegen (<svg viewBox="0 0 20 20"><path d="M11.077 15l.991-1.416a.75.75 0 1 1 1.229.86l-1.148 1.64a.748.748 0 0 1-.217.206 5.251 5.251 0 0 1-8.503-5.955.741.741 0 0 1 .12-.274l1.147-1.639a.75.75 0 1 1 1.228.86L4.933 10.7l.006.003a3.75 3.75 0 0 0 6.132 4.294l.006.004zm5.494-5.335a.748.748 0 0 1-.12.274l-1.147 1.639a.75.75 0 1 1-1.228-.86l.86-1.23a3.75 3.75 0 0 0-6.144-4.301l-.86 1.229a.75.75 0 0 1-1.229-.86l1.148-1.64a.748.748 0 0 1 .217-.206 5.251 5.251 0 0 1 8.503 5.955zm-4.563-2.532a.75.75 0 0 1 .184 1.045l-3.155 4.505a.75.75 0 1 1-1.229-.86l3.155-4.506a.75.75 0 0 1 1.045-.184z"></path></svg>).
* Link für Datenschutzerklärung
    * Laut DSGVO müssen Sie dem User die Möglichkeit geben, die Cookie-Einstellungen zu ändern. Hinterlegen Sie in Ihrer Datenschutzerklärung einen Link mit der in diesem Feld angegebenen URL.                 
* Wenn User verweigert Session-Cookie anlegen
    * Mit dieser Option können Sie festlegen, was passieren soll, wenn der User verweigert. Ist diese Option aktiviert, wird nur ein sog. Session-Cookie angelegt. Dieses wird gelöscht, sobald der User den Browser schließt. Beim nächsten Besuch auf Ihrer Subdomain wird er erneut nach Erlaubnis gefragt. Ist diese Option deaktiviert, wird ein Cookie angelegt, das die Information für ein Jahr speichert.
* Button Zustimmen
    * Hier können Sie Farbe und Text des Buttons für die Zustimmung festlegen. 
* Button Verweigern
    * Hier können Sie Farbe und Text des Buttons für das Verweigern festlegen. 
* Vorschau 
    * Hier sehen Sie sofort Ihre Änderungen und können sich Ihr Endergebnis ansehen. 