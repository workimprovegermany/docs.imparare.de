#Dokumentation

Herzlich willkommen auf der Dokumentation von imparare®. Wir arbeiten täglich daran, Ihnen den Aufenthalt auf unserer Plattform so einfach wie möglich zu gestalten.
Damit Sie sich besser zurechtfinden, haben wir diese Dokumentation für Sie erstellt. 
Derzeit befinden sich hier nur wenige Informationen. Jedoch wird diese Dokumentation stetig wachsen und stellt am Ende Ihr Handbuch dar. 
Haben Sie eine dringende Frage und Sie können hierzu nichts in der Dokumentation finden, dann kontaktieren Sie unseren <a href='https://www.imparare.de/support/support-anfrage.html' target="_blank" rel="noopener">Support</a>.