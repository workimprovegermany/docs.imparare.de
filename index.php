<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="nofollow" />
    <link rel="icon" type="image/x-icon" href="_img/icon.png">
    <meta property="og:image" content="https://www.docs.imparare.de/_img/logo-imparare-doc.svg">
    <meta name="description" content="imparare® Dokumentation">

    <link rel="stylesheet" href="_css/dist/style.min.css">

    <script src="_vendor/js/dist/vendor.min.js"></script>
    <script src="_js/dist/script.min.js"></script>

    <title>imparare | Documentation</title>


    <?php
    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    if(isset($_GET['token'])){
        $token = $_GET['token'];
        if(endsWith($token, '/')){
            $length = strlen($token)-1;;
            $token = substr($token,0, $length);
        }

        if(md5(date('Y-m-d')) != $token){
            echo 'ja';
        } else {
            echo 'nein';
        }

    }


    function startsWith($haystack, $needle) {
        return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
    }

    function nav($dir){
        $DIRECTORY_SEPARATOR = '/';
        $files = scandir($dir);
        echo "<ol>";
        foreach ($files as $key => $value)
        {
            if (!in_array($value,array(".","..")) && !startsWith($value, '_') && !startsWith($value, '.') && !startsWith($value, 'index')) {
                $name = preg_replace('/[1-9]{1,4}-/', '', $value);
                $file_name = str_replace('_', ' ', $name);



                $hash_without_ext = strtolower(preg_replace('/\\.[^.\\s]{2,4}$/', '', $name));
                $without_ext = preg_replace('/\\.[^.\\s]{2,4}$/', '', $file_name);
                $without_ext_dir = strtolower(preg_replace('/(.\/[0-9]-)/', '', $dir));

                if(is_dir($dir.$DIRECTORY_SEPARATOR.$value)){
                    echo "<li class='dir'><p><i class=\"fa fa-folder-open\"></i>&nbsp;&nbsp;".$file_name."</p>";
                    nav($dir.$DIRECTORY_SEPARATOR.$value);
                    echo "</li>";
                } else {

                    echo "<li><a href='#".$without_ext_dir.'_'.$hash_without_ext."' id='".$without_ext_dir.'_'.$hash_without_ext."' data-url='".$dir.$DIRECTORY_SEPARATOR.$value."'>".$without_ext."</a></li>";
                }


            }
        }
        echo "</ol>";
    }

    ?>

</head>
<main id="doc">

    <section>
        <a href="./"><img class="logo" src="_img/logo-imparare-doc.svg"></a>
        <hr class="marg40-b pad10-t">
    </section>
    <section id="doc_nav">
        <div id="js_rd_navi_bar"></div>
        <nav>
            <?php
            nav('.');
            ?>
        </nav>
    </section>
    <section id="doc_content">

    </section>

</main>
<footer>
    <div>
        <div class="copyrightindex"><p><?php echo "imparare.de"; ?> © <span id="copyright-year"></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://www.imparare.de/datenschutz.html" target="_blank" rel="noopener">Datenschutz</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://www.imparare.de/impressum.html" target="_blank" rel="noopener">Impressum</a> </p></div>
    </div>
</footer>

<script>
    init_doc();
</script>
</html>


