##Grunddaten
-> Live Webinar -> <i class="fa fa-edit"></i> -> Grunddaten

<div class="img_tile_view" style="background-image: url(_img/live_webinar/live-webinar-bearbeiten.png); background-position: -16px -29px;"></div>
   
* _**Adminlink:**_ Wird automatisch erstellt. Über ber diesen Linink kannst du jeder zeit in den Webinarraum eintreten, wenn ein gültiger Termin angelegt ist.
* _**Stream URL:**_ Ist automatisch ausgefüllt. Diesen Link benötigst du, um deinen Livestream zu starten. <br><i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#live_webinar_live_webinar_starten">Live Webinar starten</a>
* _**Live Stream Seite URL (Zoom):**_ Ist automatisch ausgefüllt. Diesen Link benötigst du, um deinen Livestream mit Zoom zu starten. <br><i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#live_webinar_live_webinar_starten">Live Webinar starten</a>
* _**Live Webinar Name (Pflichtfeld):**_ Gewünschten Namen eingeben.
* _**URL-Name für Subdomain:**_ Dieser Name erscheint in der URL der Anmeldeseite für das Live Webinar. Diesen kannst du leer lassen, dann wird er automatisch ausgefüllt.
* _**Video Code:**_ Wird automatisch erstellt. Diesen Code benötigst du, um deinen Livestream zu starten. <br><i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#live_webinar_live_webinar_starten">Live Webinar starten</a>
* _**Multi-Device Tracking:**_ Multi-Device Tracking wird genutzt, wenn über Affiliate-Partner ein personalisiertes Tracking gewünscht wird. So kann die Zuordnung des Affiliate-Partners sichergestellt werden. <br><i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#weitere_themen_multi-device_tracking">Multi-Device Tracking</a>
* _**Digistore24 Affiliate ID:**_ Diese Option wird nur angezeigt, wenn das Multi-Device Tracking aktiviert ist. <br><i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#weitere_themen_multi-device_tracking">Multi-Device Tracking</a>
* _**Live Webinar aktiv:**_  Mit dieser Option kannst du dein Live Webinar aktivieren oder deaktivieren. Ruft ein User einen Link eines deaktivierten Live Webinars auf, wird er auf deine persönliche Plattform (Subdomain) weitergeleitet.
