##Zusatzfunktionen
-> Live Webinar -> <i class="fa fa-edit"></i> -> Zusatzfunktionen

<div class="img_tile_view" style="background-image: url(_img/live_webinar/live-webinar-bearbeiten.png); background-position: -895px -29px;"></div>
 

###Verkaufseiten zuordnen
<img src='_img/live_webinar/live-webinar-verkaufsseiten.jpg' >
<hr>

###SMTP-Account
Wähle den SMTP-Account, über den die E-Mails versendet werden sollen.
<hr>

###HTML Meta
Wähle die Meta-Daten für die Anmeldeseite. Du kannst mit dem '+ NEUE META-DATEN'-Button neue Meta-Daten anlegen und anschließend hier auswählen.
<hr>

###Tracking Code
Hinterlege hier Tracking Code (Facebook, Google, ...) für die Anmeldeseite. Dieser wird zusätzlich zum Tracking Code unter Home -> Grundeinstellungen ausgespielt.
Du kannst Tracking Code für den Head und Body hinterlegen. Wenn du nicht möchtest, dass zusätzlich zu diesem Tracking Code, der aus den Grundeinstellungen eingebunden wird,
kannst du die Option 'Tracking Code von Grundeinstellungen einbinden' deaktivieren.
<hr>

###Zusätzliche Weblinks
In dieser Kachel hast du die Möglichkeit, weitere Weblinks für die Anmeldeseite anzulegen. Zudem siehst du hier, wie oft der Weblink aufgerufen wurde.
<hr>

###Webinar duplizieren
Hier steht dir die Möglichkeit zur Verfügung, das Webinar zu duplizieren. Hilfreich auch für Splittests.
<br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: Dein dupliziertes Webinar ist deaktiviert und muss manuell aktiviert werden. Der Name des neuen Webinars kann erst nach dem Duplizieren unter Grunddaten geändert werden.