##Live Webinar starten

###Aktuelle Anbieter
* <a href="https://zoom.us/" target="_blank" rel="noopener">Zoom</a>
* <a href="https://obsproject.com/de" target="_blank" rel="noopener">OBS Studio</a> (Open Broadcaster Software)
<hr>

###Zoom
* Login über Zoom Webseite
* Option 'Meetings'  wählen
* Ein neues Meeting planen oder ein bestehendes Meeting verwenden
* Klick auf den Namen des gewünschten Meetings <br> <img src='_img/live_webinar/live-webinar-zoom-meeting-auswaehlen.jpg' >
* 'Benutzerdefinierter Livestreaming Service' ->  'Live Stream Einstellungen kofigurieren' und Daten eingeben. <br>Die Daten, die du benötigst findest du unter: <i class="fa fa-edit"></i> -> Grunddaten <br> <img src='_img/live_webinar/live-webinar-zoom-meeting-streaming-daten.jpg' >
* Oder Daten bearbeiten <br> <img src='_img/live_webinar/live-webinar-zoom-meeting-streaming-daten-bearbeiten.jpg' >
* Auf der gleichen Seite das Meeting starten <br> <img src='_img/live_webinar/live-webinar-zoom-meeting-starten.jpg' >
* Die lokale Zoom App auf deinem PC starten
* Livestream-Übertragung starten: 'Mehr' -> 'Live auf Benutzerdefinierter Livestreaming Service' <br> <img src='_img/live_webinar/live-webinar-zoom-meeting-live-stream-starten.jpg' >
* Webinarraum öffnet sich automatisch

<span class="orange"><i class="fa fa-exclamation-triangle"></i> _HINWEIS_</span>: Bei Zoom hast du eine Latenz von ca. 20 Sekunden.
<hr>

###OBS Studio
* OBS Studio herunterladen & starten
* Neue Szene anlegen
* Neue Quelle anlegen z.B. Videoaufnahmegerät
* Einstellungen über Button 'Einstellungen' aufrufen <br> <img src='_img/live_webinar/live-webinar-obs-meeting-einstellungen.jpg' >
* Option 'Stream' und anschließend die Plattform 'Benutzerdefiniert...' auswählen <br> <img src='_img/live_webinar/live-webinar-obs-meeting-daten.jpg' >
* Server (Stream URL) & Streamschlüssel (Video Code) eingeben <br>Die Daten, die du benötigst findest du unter: <i class="fa fa-edit"></i> -> Grunddaten
* Mit dem Button 'Stream starten', das Live Webinar starten
* Webinarraum öffnet sich automatisch

<span class="orange"><i class="fa fa-exclamation-triangle"></i> _HINWEIS_</span>: Bei OBS Studio hast du eine Latenz von ca. 20 Sekunden.
