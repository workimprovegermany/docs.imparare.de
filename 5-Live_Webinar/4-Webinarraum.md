##Webinarraum
-> Live Webinar -> <i class="fa fa-edit"></i> -> Webinarraum

<div class="img_tile_view" style="background-image: url(_img/live_webinar/live-webinar-bearbeiten.png); background-position: -603px -29px;"></div>
 <br>
 <br>
<img src='_img/live_webinar/live-webinar-beschreibung-webinarraum.jpg' style="max-width: 600px;">

###Grundeinstellungen
 * _**Live Webinar Titel:**_ Der Titel des Live Webinars sehen die Teilnehmer. Dieser wird in den E-Mails verwendet und im Webinarraum angezeigt. Ist kein Titel hinterlegt, wird der Name angezeigt.
 * _**Logo:**_ Du kannst die Option aktivieren, um ein Logo im Webinarraum anzuzeigen.
 * _**Logo Upload:**_ Diese Option wird nur angezeigt, wenn du das Logo aktivierst. Wähle ein Logo aus oder lade ein neues Logo hoch.
 * _**Hintergrundfarbe:**_ Wähle eine Hintergrundfarbe für den Webinarraum. 
 * _**Abgerundete Ecken:**_ Wähle die Stärke der abgerundeten Ecken für den Video-Frame.
 * _**Schatten:**_ Aktiviere die Option, wenn der Video-Frame einen Schatten haben soll.
 * _**Teilnehmer anonym:**_ Mit dieser Option kannst du auswählen, ob die Teilnehmer mit Namen angezeigt werden oder nur die Anzahl.
<hr>

###Vorschaubild
Wähle ein Vorschaubild aus oder lade ein neues Bild hoch. Dieses Bild wird im Webinarraum angezeigt, wenn das Live Webinar noch nicht gestartet, jedoch der Webinarraum bereits für die Teilnehmer geöffnet ist. Zudem wird das Bild in der Übersicht angezeigt.
<hr>

###URL nach Webinar
Hier kannst du eine URL auswählen, zu der die Teilnehmer nach dem Live Webinar weitergeleitet werden. Wenn du noch keine URL angelegt hast, kannst du dies über den '+ NEUE URL'-Button machen.
Ist keine URL ausgewählt, sieht der Teilnehmer nach dem Webinar einen 'Beendet' Screen.
<hr>

###Angebots PopUp
Das Angebots PopUp wird mit dem Pagebuilder gebaut. Die maximale Höhe beträgt 600px. Alles was darüber hinaus geht muss der User scrollen.
Es kann während des Live Webinars aktiviert und deaktiviert werden.
<hr>

###Moderatoren
In dieser Kachel kannst du Moderatoren für einen Termin anlegen. Die Moderatoren erhalten automatisch eine E-Mail mit dem Link zum Webinarraum.
Dem Moderator stehen die gleichen Optionen zur Verfügung wie dem Admin. Zuusätzlich kann er 10 Minuten vor dem Start in den Webinarraum eintreten.