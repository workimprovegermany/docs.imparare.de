##Neues Live Webinar anlegen
-> Home -> Live Webinar
<img src='_img/live_webinar/live-webinar-anlegen.png' >

<i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#live_webinar_grunddaten">Grunddaten</a>
<br>
<br>
Nach dem Ausfüllen der Grunddaten und dem Speichern gelangst du über den 'ZURÜCK'-Button auf die Übersicht.
<br>
<br>
<div class='warning'>
    Hinweis: Solltest du dein neu angelegtes Webinar nicht auf der Übersicht sehen, ist es eventuell nicht aktiv. In diesem Fall musst du die inaktiven Webinare auswählen.
    <br>
    <br>
    <img src='_img/live_webinar/aktive-live-webinare.png' > 
</div>