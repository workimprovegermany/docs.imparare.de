##Anmeldeseite
-> Live Webinar -> <i class="fa fa-edit"></i> -> Anmeldeseite

<div class="img_tile_view" style="background-image: url(_img/live_webinar/live-webinar-bearbeiten.png); background-position: -309px -29px;"></div>
 

###Pagebulider (Landingpage)
####Landingpage Grunddaten
 * _**Highlightfarbe:**_ Du kannst eine Farbe hinterlegen, die du dann in den Texteditoren als Text- und Hintergrundfarbe findest.
 * _**Anzahl Termine:**_ Hier kannst du auswählen, wie viele Termine der Kunde im Anmeldeformular zur Auswahl hat.
 * _**Anmeldung auf Overlay:**_ Du kannst auswählen, ob das Anmeldeformular direkt auf der Landingpage integriert sein soll oder auf dem Overlay. Wenn du möchtest, dass das Anmeldeformular auf dem Overlay integriert wird, musst du diese Option aktivieren. Du erhältst automatisch einen Button auf deiner Landingpage, um das Overlay zu öffnen. <br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: Du hast bereits angefangen, deine Landingpage zu bauen, und möchtest diese Option ändern, dann musst du deine Landingpage und ggf. das Overlay erneut überprüfen und ebenfalls erneut speichern. Ansonsten können Fehler auftreten! 
 * _**Overlay mit Exit PopUp:**_ Ist diese Option aktiv und der User möchte die Seite verlassen, ohne sich zum Live Webinar angemeldet zu haben, erscheint das Overlay automatisch noch mal als PopUp. <br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>:  Um dieses Feature zu nutzen, muss die Option 'Anmeldung auf Overlay' aktiv sein.
 * _**Autom. Zwischenspeichern:**_ Wenn du diese Option aktivierst, werden deine Änderungen im Pagebuilder deiner Anmeldeseite und dem Overlay alle 5 Minuten automatisch gespeichert.

####Landingpage bearbeiten
<i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#pagebuilder_allgemein">Pagebuilder</a>

####Overlay bearbeiten
<i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#pagebuilder_allgemein">Pagebuilder</a>
<hr>

###iFrame
Wenn du die Anmeldung auf dem Overlay aktiviert hast, kannst du es als iFrame auf deiner externen Ladingpage einbinden.
Gestalte dir deinen Button nach deinen Wünschen. Jede Einstellung, die du änderst, siehst du in der Vorschau am Ende der Seite.
Bist du fertig mit dem Design deines Button, musst du es nur noch speichern und den iFrame Code am Ende der Seite kopieren und in deine externe Landingpage einfügen.  
Es gibt zwei unterschiedliche Codes, die du auf deiner externen Seite einbinden musst.
* _**iFrame Code Button**_: Dieser Code erzeugt deinen Button. Er kann so oft du möchtest in deine externe Seite eingebunden werden.
* _**iFrame Code Script**_: Dieser Code darf nur *einmal* in deine externe Seite eingebunden werden und das ganz am Ende bzw. nach dem letzten eingebundenen Button.
 
<br>

**Übergabeparameter hinzufügen**  
Für das Zusatzfeld, um welches du dein Anmeldeformular erweitern kannst, kannst du einen festen Übergabewert hinterlegen.
Das kann sinnvoll sein, wenn du z. B. einen Beraternamen übergeben möchtest.
Um den Übergabeparameter festzulegen, muss der iFrame Code manuell angepasst werden.   
Das Attribut **data-memo="[value]"** muss im script-Tag ergänzt werden.   
Beispiel:

<code class="language-html" data-lang="html">
<span>&lt;div</span> <span>style=</span><span>"text-align: center;"</span><span>&gt;</span>
<span>&lt;button</span> <span>type=</span><span>"button"</span> <span>id=</span><span>"im_btn_show_iframe"</span> <span>data-webinar-shorty=</span><span>"[shorty]"</span><span>&gt;</span> <span>Jetzt anmelden</span> <span>&lt;/button</span><span>&gt;</span>
<span class="nt">&lt;script</span> <span class="nt">src=</span><span class="nt">"[subdomain]/iframe/live"</span> <span class="s">data-memo=</span><span class="s">"Max_Mustermann"</span><span class="nt">&gt;</span><span class="nt">&lt;/script&gt;</span>
<span>&lt;/div&gt;</span>
</code>

<br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: Der Unterstrich (_) im Namen wird durch ein Leerzeichen ersetzt. So würde hier im Beispiel der Name im Zusatzfeld als *Max Mustermann* angezeigt werden.
<hr>

###Kontaktformular
* _**Anrede:**_ Hier kannst du entscheiden, wie die Anrede an deine Teilnehmer sein soll. Entweder Du oder Sie. Das betrifft E-Mails und Overlays, die du nicht selbst gestalten kannst.
* _**Double Opt-In:**_ Deaktivierst du den Double Opt-In, wird der neue Teilnehmer nach seiner Anmeldung nicht aufgefordert, seine E-Mail-Adresse zu bestätigen.<br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: Deaktiviert ist nicht DSGVO konform!
* _**E-Mail Anbieter:**_ Hier kannst du den Anbieter für die Double OptIn Funktion auswählen, wenn dieser nicht über die API automatisiert ist.
* _**Exportieren:**_ Ist diese Option aktiviert, werden deine Teilnehmerdaten zu deinem Drittanbieter exportiert.
* _**Abweichende Listen-ID:**_ Hier kannst du eine abweichende List-ID angeben, in die die Teilnehmerdaten exportiert werden sollen.

<hr>

###Webinartermine
In dieser Kachel kannst du deine Webinartermine verwalten. Lege neue Termine an oder lösche vergangene Termine.   
Um einen neuen Termin anzulegen, wähle ein Datum mit dem Datepicker und gebe eine Uhrzeit ein. Mit dem 'ANLEGEN'-Button wird der neue Termin angelegt.

<hr>

###Layout mit Exit PopUp
In dieser Kachel kannst du das Exit PopUp aktivieren. Wähle hierfür auch das gewünschte, bereits angelegt PopUp in der Selectbox aus und entscheide, ob der Link sich im selben oder in einem neuen Fenster öffnet.
Wenn du noch kein Exit PopUp angelegt hast, kannst du dies über den Button 'NEUER EXIT POPUP' machen. 
<br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: Kann mit der Option 'Overlay mit Exit PopUp' kollidieren.

<hr>

###Layout mit Besucher PopUp
Hier kannst du auswählen, welches Besucher PopUp auf deiner Anmeldeseite erscheinen soll.<br>
<i class="fa fa-eye"></i> siehe: <a class="js_doc_link" href="#weitere_themen_besucher_popup">Besucher PopUp</a>
<hr>

###Vorschau
Wenn deine Anmeldeseite mit dem Pagebuilder erstellt ist oder importiert wurde und alle Einstellungen getätigt sind, kannst du das Ergebnis in der Vorschau anschauen.
<br><span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: In der Vorschau stehen dir nicht alle Funktionen zur Verfügung z. B. Anmeldung über das Formular.
<br><br><img src='_img/live_webinar/live-webinar-anmeldeseite-vorschau.jpg' >
<hr>

###Übergebeparameter
Für das Zusatzfeld, um welches du dein Anmeldeformular erweitern kannst, kannst du einen festen Übergabewert hinterlegen.
Das kann sinnvoll sein, wenn du z. B. einen Beraternamen übergeben möchtest. Es kann aber auch für jeden beliebigen anderen Wert verwendet werden.
Um den Wert dem Formular zu übergeben, musst du an deine URL den Parameter **memo** anhängen.
Eine Video-Anleitung zu diesem Thema findest du hier: <a href="https://youtu.be/-Tl1XAKuD6w" target="_blank" rel="noopener">zur Video-Anleitung</a>.  
<br>
_**Anhängen an Short-Link**_  
Beispiel:
<code class="language-html" data-lang="html">
<span>https://[subdomain]/[shorty]</span><span class="s">?memo=Max_Mustermann</span>
</code>
<br> 
_**Anhängen an Long-Link**_  
Beispiel:
<code class="language-html" data-lang="html">
<span>https://[subdomain]/lw/[name]/[shorty]</span><span class="s">?memo=Max_Mustermann</span>
</code>
<br>
<span class="orange"><i class="fa fa-exclamation-triangle"></i> _ACHTUNG_</span>: Der Unterstrich (_) im Namen wird durch ein Leerzeichen ersetzt. So würde hier im Beispiel der Name im Zusatzfeld als *Max Mustermann* angezeigt werden.
