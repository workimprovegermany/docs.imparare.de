#Live Webinar
-> Home -> Live Webinar

<img src='_img/live_webinar/live-webinar-beschreibung-uebersicht.jpg' >
<hr>
<br>
<br>

##Das könntest du suchen & FAQ
###Webinar Name vs. Webinar Titel
Der **Webinar Name** ist für dich zur Orientierung. Dieser wird auf der Übersicht angezeigt und sollte möglichs einfach und kurz gehalten werden.
Der **Webinar Titel** ist der Titel des Live Webinares, den die Teilnehmer sehen. Dieser wird in den E-Mails verwendet und im Webinarraum angezeigt. Sollte der Titel leer sein, wird der Name in den E-Mails und im Webinarraum angezeigt.
* Live Webinar Name: <i class="fa fa-edit"></i> -> Grunddaten
* Live Webinar Titel: <i class="fa fa-edit"></i> -> Webinarraum -> Grundeinstellungen

###Webinarraum Vorschau
Als Admin kannst du jederzeit in den Webinarraum eintreten, wenn ein aktueller Termin angelegt ist.
Den Admin-Link findest du unter: <br><i class="fa fa-edit"></i> -> Grunddaten.

###Export Teilnehmerdaten aktivieren
<i class="fa fa-edit"></i> -> Anmeldeseite -> Kontaktformular

###Vorschaubild ändern
<i class="fa fa-edit"></i> -> Webinarraum -> Vorschaubild

###Vorschau Anmeldeseite
<i class="fa fa-edit"></i> -> Anmeldeseite -> Vorschau