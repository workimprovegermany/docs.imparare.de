docReady(function() {
    if(document.getElementById('js_rd_navi_bar')){
        init_rd_navi_bar();
    }
});

function init_rd_navi_bar(){
    document.getElementById('js_rd_navi_bar').classList.add('rd_navi_bar');
    document.getElementById('js_rd_navi_bar').innerHTML = '<span class="cls"></span><span></span><span class="cls"></span>';

    document.getElementById('js_rd_navi_bar').addEventListener("click", function (ev) {
        toggle_Class(this, 'open');
    });

    let link_elem = document.getElementsByTagName('nav')[0].getElementsByTagName('a');
    for(let i= 0; i < link_elem.length; i++){
        link_elem[i].addEventListener('click', function (ev) {
            toggle_Class(document.getElementById('js_rd_navi_bar'), 'open');
        }) 
    }
}