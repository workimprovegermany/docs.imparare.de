function addClass(className, elemente) {
    for(let i = 0; i < elemente.length; i++){
        elemente[i].classList.add(className);
    }
}

function add_attribute(elemente, attrName, value) {
    for(let i = 0; i < elemente.length; i++){
        elemente[i].setAttribute(attrName, value)
    }
}

function remove_attribute(elemente, attrName) {
    for(let i = 0; i < elemente.length; i++){
        elemente[i].removeAttribute(attrName)
    }
}

function addClassToElements(object, cssClass) {
    if(Array.isArray(object)){
        for(let i = 0; i < object.length; i++){
            object[i].classList.add(cssClass);
        }
    } else {
        object.classList.add(cssClass);
    }

}

function removeClassToElements(object, cssClass) {
    if(Array.isArray(object)){
        for(let i = 0; i < object.length; i++){
            object[i].classList.remove(cssClass);
        }
    } else {
        object.classList.remove(cssClass);
    }
}

function toggle_Class(element, cssClass) {
    if(hasClass(element, cssClass)){
        element.classList.remove(cssClass);
    } else {
        element.classList.add(cssClass);
    }
}

function isNotEmpty(value) {
    let bool;
    ('' === value || null === value  || undefined === value) ? bool = false : bool = true;
    return bool;
}

/**
 *
 * @param element
 * @param addClass Klasse, die hinzugefügt werden soll
 * @param removeClass Klasse, die entfernt werden soll
 */
function changeClass(element, addClass, removeClass){
    element.classList.remove(removeClass);
    element.classList.add(addClass);
}

function hasClass(element, cssClass){
    let classList = element.classList,
        bool = false;
    for (let i = 0; i < classList.length; i++){
        if(classList[i] === cssClass){
            bool = true;
            break;
        }
    }
    return bool;
}

/**
 *
 * @param element
 * @param hideClass Wird angezeigt, wenn element AUSgebelndet ist
 * @param showClass Wird angezeigt, wenn element EINgeblendet ist
 */
function showMore(element, hideClass, showClass){
    if(hasClass(element, 'hide')){
        element.classList.remove('hide');
        changeClass(element, showClass, hideClass);
    } else {
        element.classList.add('hide');
        changeClass(element, hideClass, showClass);

    }
}

/**
 * Entfernt alle doppelten Elemente im Array
 * @param a
 * @returns {*}
 */
function uniqArray(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

/**
 * Sortiert das Array nach Datum - ältestes zuerst
 * @param array
 * @returns {*}
 */
function sortArrayByDate(array){
    array.sort(function(a, b){
        a = new Date(a.datum.replace(/-/g, "/"));
        b = new Date(b.datum.replace(/-/g, "/"));
        return a>b ? 1 : a<b ? -1 : 0;
    });
    return array;
}

function get_other_date_from_today(days, month, years){
    let current_date = new Date();
    current_date.setDate(current_date.getDate() + days);
    current_date.setMonth(current_date.getMonth() + month);
    current_date.setFullYear(current_date.getFullYear() + years);
    return current_date.toISOString();
}

function get_last_seven_days() {
    let tmp_array = [];
    for(let i = 6; i >=0; i--){
        let days_ago = i * (-1);
        tmp_array.push(get_other_date_from_today(days_ago, 0,0))
    }
    return tmp_array;
}

function get_short_name_day(date_string){
    let day_num = new Date(date_string).getDay();
    return app.array_day_names_short[day_num];
}

/**
 * Kopiert einen Wert in die Zwischenablage
 * @param copyValue
 */
function copyToClipboard(copyValue) {
    const textArea = document.createElement('textarea');
    textArea.textContent = copyValue;
    document.body.append(textArea);
    textArea.select();
    document.execCommand("copy");
    textArea.parentNode.removeChild(textArea);
}


function add_event_listener_click(elemente, event_function) {
    for(let i = 0; i < elemente.length; i++){
        elemente[i].addEventListener('click', event_function);
    }
}

function find_ancestor_width_class (el, css) {
    while ((el = el.parentElement) && !el.classList.contains(css));
    return el;
}

function find_ancestor_width_id_like (el, id) {
    while ((el = el.parentElement) && el.id.indexOf(id) === -1);
    return el;
}

function set_cookie(name, value){
    document.cookie = name + "=" + value;
}

function get_cookie(start, end){
    return document.cookie.substring(start, end).split('=')[1];
}

function get_url_anchor() {
    let currentUrl = document.URL,
        urlParts   = currentUrl.split('#');

    return (urlParts.length > 1) ? urlParts[1] : null;
}

function read_markdown_file(id, url, after_func = null)
{
    let rawFile = new XMLHttpRequest();
    rawFile.open("GET", url, true);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status === 0)
            {
                let allText = rawFile.responseText,
                    converter = new showdown.Converter();

                document.getElementById(id).innerHTML =  converter.makeHtml(allText);

                let el = document.getElementById(id).getElementsByTagName('code');
                for(let e of el){
                    if(!e.className.match('language-html'))
                        return;
                    let div = document.createElement('div');
                    div.innerHTML = e.innerText;
                    e.innerHTML = div.innerHTML;
                }

                if(after_func)
                    after_func();
            }
        }
    };
    rawFile.send(null);
}