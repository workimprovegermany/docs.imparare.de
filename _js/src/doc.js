function init_doc(){
    let WELCOMEPAGE_URL = '_willkommen.md';
    let link_elem = document.getElementsByTagName('nav')[0].getElementsByTagName('a');
    for(let i= 0; i < link_elem.length; i++){
        link_elem[i].addEventListener('click', load_data_from_link);
    }

    let anchor = get_url_anchor();
    if(anchor){
        read_markdown_file('doc_content', document.getElementById(anchor).getAttribute('data-url'), register_links_doc);
        document.getElementById(anchor).classList.add('active');

    } else {
        read_markdown_file('doc_content', WELCOMEPAGE_URL);
    }
}

function load_data_from_link(ev){
    read_markdown_file('doc_content', this.getAttribute('data-url'), register_links_doc);
    let elem = document.getElementsByTagName('nav')[0].getElementsByClassName('active')[0];
    if(elem)
        elem.classList.remove('active');

    this.classList.add('active');
}

function load_data_from_doc_link(ev){
    let id = this.href.split('#')[1];
    let nav_elem = document.getElementById(id);
    if(!nav_elem){
        ev.preventDefault();
        return;
    }
    let doc_url = nav_elem.getAttribute('data-url');
    read_markdown_file('doc_content', doc_url, register_links_doc);
    let elem = document.getElementsByTagName('nav')[0].getElementsByClassName('active')[0];
    if(elem)
        elem.classList.remove('active');

    this.classList.add('active');
}

function register_links_doc() {
    let links = document.getElementsByClassName('js_doc_link');
    for(let i= 0; i < links.length; i++){
        links[i].addEventListener('click', load_data_from_doc_link);
    }
}